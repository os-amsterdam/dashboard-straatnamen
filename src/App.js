import { useState, useEffect } from "react";
import { GlobalStyle, ThemeProvider, Row } from "@amsterdam/asc-ui";
import Selector from "./components/Selector";
import MapAms from "./components/MapAms";
import Info from "./components/Info";

const BAG = "https://api.data.amsterdam.nl/bag/v1.1/openbareruimte/";

function App() {
  const [selected, setSelected] = useState({
    display: "Dam",
    id: "0363300000003186",
  });

  useEffect(() => {
    const urlParams = new URLSearchParams(window.location.search);
    const selectedParam = urlParams.get("gebied");

    if (selectedParam && selectedParam.length === 16) {
      let isMounted = true;

      fetch(`${BAG}${selectedParam}/`)
        .then((response) => response.json())
        .then((json) => {
          if (isMounted) {
            json?._display &&
              setSelected({
                display: json._display,
                id: json.openbare_ruimte_identificatie,
              });
          }
        });

      return () => {
        isMounted = false;
      };
    }
  }, []);

  return (
    <ThemeProvider>
      <GlobalStyle />
      <Row hasMargin={false} style={{ padding: "4px" }}>
        <Selector selected={selected} setSelected={setSelected} />
        <MapAms code={selected?.id} setSelected={setSelected} />
        {selected && <Info selected={selected} setSelected={setSelected} />}
      </Row>
    </ThemeProvider>
  );
}

export default App;
