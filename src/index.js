import { StrictMode } from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';

// render micro frontend function
window.renderApp = () => {
  const container = document.getElementById('micro-frontend')
  if (container) {
    ReactDOM.render(
      <StrictMode>
        <App />
      </StrictMode>,
      container
    );
    reportWebVitals();
  }
};

// Mount to root if it is not a micro frontend
if (!document.getElementById('micro-frontend')) {
  ReactDOM.render(
    <StrictMode>
      <App />
    </StrictMode>,
    document.getElementById('root')
  );
}

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
