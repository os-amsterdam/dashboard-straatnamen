import { ResponsiveContainer } from "recharts/lib/component/ResponsiveContainer"
import { BarChart } from "recharts/lib/chart/BarChart"
import { Bar as BarRecharts } from "recharts/lib/cartesian/Bar"
import { XAxis } from "recharts/lib/cartesian/XAxis"
import { YAxis } from "recharts/lib/cartesian/YAxis"
import { LabelList } from "recharts/lib/component/LabelList"
import LoadingAnimation from '../LoadingAnimation/LoadingAnimation'
import styled from 'styled-components'

const RechartsCSSGridFix = styled.div`
  position: relative;
  width: 100%;
  height: ${props => props.height}px;
`

const StyledResponsiveContainer = styled(ResponsiveContainer)`
  position: absolute;
`

const Bar = ({ data, height = 500, color = '#004699', isLoading, zeroText = '', dataLabel }) => {
  return (
    <RechartsCSSGridFix height={height}>
      <StyledResponsiveContainer width='100%' height={height}>
        {!isLoading ?
        <BarChart data={data} margin={{ top: 0, right: 32, bottom: 5 }} layout='vertical' barCategoryGap={14}>
          <XAxis type='number' hide />
          <YAxis
            width={132}
            type='category'
            dataKey='name'
            axisLine={false}
            tickSize={0}
            tick={{ fontSize: '14px', fill: '#767676', transform: 'translate(-4, 0)' }}
          />
          <BarRecharts
            dataKey='value'
            fill={color}
            maxBarSize={36}
            animationDuration={350}
          >
            <LabelList
              dataKey='value'
              position='right'
              offset={4}
              style={{ fontSize: '12px', fill: '#767676' }}
              formatter={
                value => value === 0 ? `${value} ${zeroText}` : (Math.round(value * 10) / 10).toLocaleString('nl-NL')
              }
            />
          </BarRecharts>
        </BarChart> :
        <LoadingAnimation />}
      </StyledResponsiveContainer>
    </RechartsCSSGridFix>
  )
}

export default Bar