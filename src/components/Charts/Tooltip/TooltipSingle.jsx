import React from 'react';
import styled from 'styled-components';

const StyledTooltip = styled.div`
  background-color: white;
  border: 1px solid #979797;
  border-radius: 1px;
  box-shadow: 0 2px 4px 0 #979797;
  padding: 8px 12px;
  min-width: 120px;
`

const StyledTooltipValue = styled.span`
  font-weight: bold;
  font-size: 18px;
`

const StyledTooltipInfo = styled.span`
  font-size: 14px;
  margin-bottom: 4px;
`

const TooltipSingle = ({ active, payload, tooltipLabel }) => {
  if (active && payload) {
    return (
      <StyledTooltip>
        <StyledTooltipValue>{payload[0].value}</StyledTooltipValue>
        <StyledTooltipInfo>{tooltipLabel ? `${tooltipLabel(payload[0].payload.name)}` : ` ${payload[0].payload.name}`}</StyledTooltipInfo>
      </StyledTooltip>
    )
  } else return null;
}

export default TooltipSingle;