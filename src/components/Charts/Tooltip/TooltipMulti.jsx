import React from 'react';
import styled from 'styled-components';

const StyledTooltip = styled.div`
  background-color: white;
  border: 1px solid #979797;
  border-radius: 1px;
  box-shadow: 0 2px 4px 0 #979797;
  padding: 8px 12px;
  min-width: 120px;
`

const StyledTooltipTitle = styled.p`
  font-size: 18px;
  margin: 0;
  margin-bottom: 8px;
`

const StyledTooltipList = styled.ul`
  list-style: none;
  margin: 0;
  padding: 0;
`

const StyledTooltipItem = styled.li`
  position: relative;
  margin-top: 2px;
  margin-bottom: 2px;
`

const StyledTooltipValue = styled.span`
  margin-left: ${({ multi }) => multi ? '12px' : '0px'};
  font-weight: bold;
  font-size: 18px;
`

const StyledTooltipInfo = styled.span`
  font-size: 14px;
  margin-bottom: 4px;
`

const TooltipMulti = ({ active, payload, tooltipLabel, tooltipSort }) => {
  if (active && payload[0]) {
    const sortedItems = tooltipSort ? payload.sort((a, b) => (b.value-a.value)) : payload;
    const multi = payload.length > 1
    return (
      <StyledTooltip>
        <StyledTooltipTitle>{payload[0].payload.name}</StyledTooltipTitle>
        <StyledTooltipList>
          {sortedItems.map(item => (
            <StyledTooltipItem key={item.name}>
              {multi &&
                <svg width={8} height={12} pointerEvents='none' style={{ position: 'absolute', top: '3px' }}>
                  <rect stroke='none' fill={item.color} width={12} height={8} y={3} />
                </svg>
              }
              <StyledTooltipValue multi={multi}>{item.value}</StyledTooltipValue>
              <StyledTooltipInfo>{tooltipLabel ? `${tooltipLabel(item.name)}` : ` ${item.name}`}</StyledTooltipInfo>
            </StyledTooltipItem>
          ))}
        </StyledTooltipList>
      </StyledTooltip>
    )
  } else return null;
}

export default TooltipMulti;