import { useState, useRef, useEffect } from "react";
import {
  themeColor,
  List,
  ListItem,
  Button,
  themeSpacing,
} from "@amsterdam/asc-ui";
import { ChevronRight } from "@amsterdam/asc-assets";
import styled from "styled-components";
import useTraverseList from "./useTraverseList";

const SelectorContainer = styled.div`
  position: relative;
  width: 100%;
  background-color: ${themeColor("secondary", "main")};
  display: flex;
  flex-wrap: wrap;
  align-items: center;
  padding-top: 16px;
  padding-bottom: 16px;
  padding-left: 24px;
  padding-right: 24px;
  color: white;
`;

const StyledLabel = styled.label`
  font-weight: 700;
  font-size: 20px;
  margin-right: 12px;
  line-height: 28px;
`;

const TextFieldContainer = styled.div`
  position: relative;
  margin: 8px 0px;
  max-width: 100%;
`;

const StyledInput = styled.input`
  appearance: none;
  font-size: 16px;
  border: solid 1px ${themeColor("tint", "level5")};
  border-radius: 0;
  box-sizing: border-box;
  line-height: 18px;
  padding: ${themeSpacing(2)};
  height: 44px;
  width: 400px;
  max-width: 100%;
`;

const Autosuggest = styled.div`
  position: absolute;
  z-index: 1100;
  display: ${({ isOpen }) => (isOpen ? "block" : "none")};
  background-color: white;
  border: 1px solid ${themeColor("tint", "level5")};
  margin-right: 37px;
  width: 100%;
`;

const StyledListItem = styled(ListItem)`
  margin-bottom: 0;
`;

const StyledButton = styled(Button)`
  width: 100%;
  padding: ${themeSpacing(2, 2)};
  color: black;
  font-weight: normal;

  &.active,
  &:hover {
    background-color: ${themeColor("tint", "level2")};
    color: black;
  }
  & path {
    fill: black !important;
  }
`;

const StyledList = styled(List)`
  margin: 0;
`;

const Message = styled.div`
  color: ${themeColor("tint", "level5")};
  padding: ${themeSpacing(3)};
`;

const URL =
  "https://api.pdok.nl/bzk/locatieserver/search/v3_1/suggest?fl=weergavenaam%20openbareruimte_id&fq=bron%3ABAG&fq=type%3Aweg&fq=woonplaatsnaam%3A%20Amsterdam%2C%20Weesp&rows=10&wt=json&q=";

const cleanInput = (str) => str.replace("(", "").replace(")", "");
const highlight = (str, substr) =>
  str.replace(new RegExp(substr, "gi"), (str) => `<strong>${str}</strong>`);

const Selector = ({ selected, setSelected }) => {
  const [isOpen, setIsOpen] = useState(false);
  const [data, setData] = useState();
  const [input, setInput] = useState("");

  useEffect(() => {
    if (isOpen) {
      let isMounted = true;

      fetch(`${URL}${input}`)
        .then((response) => response.json())
        .then((json) => {
          if (isMounted) {
            const normalized = json.response.docs.map((item) => ({
              display: item.weergavenaam,
              id: item.openbareruimte_id,
            }));

            setData(normalized);
          }
        });

      return () => {
        isMounted = false;
      };
    }
  }, [isOpen, input]);

  useEffect(() => {
    if (selected) {
      setInput(selected.display ? selected.display : "");
    }
  }, [selected]);

  const handleChange = (e) => {
    const value = e.target.value;

    // set input text
    setInput(value);

    // open autosuggest if input > 2 letters
    value.length > 2 ? setIsOpen(true) : setIsOpen(false);
  };

  const onEscape = () => {
    setIsOpen(false);
    setInput("");
  };

  const onEnter = (element) => {
    if (element) {
      setSelected({
        display: element.textContent,
        id: element.value,
      });
      setInput(element.textContent);
      setIsOpen(false);
    }
  };

  const onFocus = () => {
    input.length > 2 && setIsOpen(true);
  };

  const onBlur = () => {
    // Arbitrary 300 ms timeout here, needed since onBlur is triggered before the user can actually click on a link
    setTimeout(() => {
      setIsOpen(false);
    }, 300);
  };

  const handleClick = (name, code) => {
    setSelected({
      display: name,
      id: code,
    });
    setInput(name);
    setIsOpen(false);
  };

  const ref = useRef(null);

  const { keyDown } = useTraverseList(
    ref,
    (activeElement, list) => {
      list.forEach((el) => {
        el.classList.remove("active");
      });
      activeElement.classList.add("active");
    },
    {
      rotating: true,
      directChildrenOnly: false,
      horizontally: false,
    },
    onEscape,
    onEnter
  );

  return (
    <SelectorContainer>
      <StyledLabel id="search-field-label" htmlFor="search-field">
        Zoek uw straat
      </StyledLabel>
      <TextFieldContainer
        role="combobox"
        aria-expanded={isOpen}
        aria-owns="autosuggest"
        aria-haspopup="listbox"
      >
        <StyledInput
          id="search-field"
          type="text"
          value={input}
          onChange={handleChange}
          onKeyDown={keyDown}
          onFocus={onFocus}
          onBlur={onBlur}
          placeholder="Vul een straatnaam in en ontdek de betekenis"
          aria-autocomplete="list"
          aria-controls="autosuggest"
          autoComplete="off"
        />
        <Autosuggest isOpen={isOpen} ref={ref}>
          <StyledList
            id="autosuggest"
            aria-labelledby="search-field-label"
            role="listbox"
          >
            {data &&
              data.length > 0 &&
              data.slice(0, 10).map((entry) => {
                return (
                  <StyledListItem key={entry.id}>
                    <StyledButton
                      variant="textButton"
                      iconSize={14}
                      iconLeft={<ChevronRight />}
                      onClick={() => handleClick(entry.display, entry.id)}
                      value={entry.id}
                      tabIndex={-1}
                    >
                      <span
                        dangerouslySetInnerHTML={{
                          __html: highlight(entry.display, cleanInput(input)),
                        }}
                      />
                    </StyledButton>
                  </StyledListItem>
                );
              })}
          </StyledList>
          {data && data.length > 10 ? (
            <Message>
              Nog {data.length - 10} {data.length === 11 ? "straat" : "straten"}{" "}
              gevonden. Verfijn je zoekopdracht.
            </Message>
          ) : null}
          {data && data.length === 0 ? (
            <Message>Geen resultaten gevonden.</Message>
          ) : null}
        </Autosuggest>
      </TextFieldContainer>
    </SelectorContainer>
  );
};

export default Selector;
