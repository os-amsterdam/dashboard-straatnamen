import { Fragment, useState, useEffect } from "react";
import styled from "styled-components";
import {
  themeSpacing,
  themeColor,
  Row,
  Heading,
  Paragraph,
  List,
  ListItem,
  Button,
  breakpoint,
} from "@amsterdam/asc-ui";
import { ChevronRight } from "@amsterdam/asc-assets";
import Details from "./Details";
import oisData from "../data";

const BAG = "https://api.data.amsterdam.nl/bag/v1.1/openbareruimte/";

const LIST_CONFIG = [
  {
    label: "Reden naamgeving",
    propName: "reden_naamgeving",
  },
  {
    label: "Geboortejaar",
    propName: "geboortejaar",
  },
  {
    label: "Sterfjaar",
    propName: "sterfjaar",
  },
  {
    label: "Oorsprong",
    propName: "oorsprong",
  },
];

const InfoContainer = styled.div`
  width: 100%;
  border: 2px solid ${themeColor("tint", "level3")};
  margin-bottom: ${themeSpacing(20)};
`;

const Column = styled.div`
  width: 50%;
  padding: ${themeSpacing(6)};

  @media screen and ${breakpoint("max-width", "laptop")} {
    width: 100%;
  }
`;

const StyledHeading = styled(Heading)`
  display: block;
  margin-top: 0;
`;

const RedHeading = styled(Heading)`
  color: ${themeColor("secondary")};
  margin-top: 0;
  margin-bottom: ${themeSpacing(5)};
`;

const MeaningListItem = styled(ListItem)`
  border-top: 1px solid ${themeColor("tint", "level3")};
  padding-top: ${themeSpacing(2)};
  padding-bottom: ${themeSpacing(2)};
  margin: 0;

  &:last-child {
    border-bottom: 1px solid ${themeColor("tint", "level3")};
  }
`;

const Cell = styled.div`
  width: 50%;
  display: inline-block;
  vertical-align: top;

  @media screen and ${breakpoint("max-width", "mobileL")} {
    width: 100%;
    margin-bottom: ${themeSpacing(2)};
  }
`;

const Value = styled(Cell)`
  font-weight: 700;
`;

const Message = styled.div`
  color: ${themeColor("tint", "level5")};
  margin-top: 12px;
`;

const URL =
  "https://api.pdok.nl/bzk/locatieserver/search/v3_1/suggest?fl=weergavenaam%20openbareruimte_id&fq=bron%3ABAG&fq=type%3Aweg&fq=woonplaatsnaam%3A%20Amsterdam%2C%20Weesp&rows=10&wt=json&q=";

const Info = ({ selected, setSelected }) => {
  const [bagData, setBagData] = useState();
  const [selectedOisData, setSelectedOisData] = useState();
  const [related, setRelated] = useState();

  useEffect(() => {
    if (selected.id) {
      setSelectedOisData(oisData?.find((item) => item.code === selected.id));
    }
  }, [selected]);

  useEffect(() => {
    let isMounted = true;

    if (selectedOisData) {
      fetch(`${URL}${selectedOisData.naam}`)
        .then((response) => response.json())
        .then((json) => {
          if (isMounted) {
            const normalized = json.response.docs.map((item) => ({
              display: item.weergavenaam,
              id: item.openbareruimte_id,
            }));
            const filtered = normalized.filter(
              (item) => item.id !== selectedOisData.code
            );
            setRelated(filtered);
          }
        });
    }

    return () => {
      isMounted = false;
    };
  }, [selectedOisData]);

  useEffect(() => {
    if (selected.id) {
      let isMounted = true;

      fetch(`${BAG}${selected.id}/`)
        .then((response) => response.json())
        .then((json) => {
          if (isMounted) {
            setBagData(json);
          }
        });

      return () => {
        isMounted = false;
      };
    }
  }, [selected]);

  return (
    <InfoContainer>
      <Row hasMargin={false}>
        <Column>
          <StyledHeading as="h2">
            Waar komt de naam {selected.display} vandaan?
          </StyledHeading>
          {selectedOisData?.beschrijvingUva?.length > 0 ? (
            selectedOisData.beschrijvingUva.map((text) => (
              <Paragraph key={text}>{text}</Paragraph>
            ))
          ) : (
            <Paragraph>
              {bagData?.omschrijving
                ? bagData.omschrijving
                : "Geen informatie beschikbaar."}
            </Paragraph>
          )}
          {related?.length > 0 && (
            <Fragment>
              <RedHeading as="h3">
                Openbare ruimten vernoemd naar {selectedOisData?.naam}
              </RedHeading>
              <List style={{ margin: "0" }}>
                {related.slice(0, 5).map((entry) => (
                  <ListItem key={entry.id}>
                    <Button
                      variant="textButton"
                      iconSize={14}
                      iconLeft={<ChevronRight />}
                      onClick={() =>
                        setSelected({
                          display: entry.display,
                          id: entry.id,
                        })
                      }
                    >
                      {entry.display}
                    </Button>
                  </ListItem>
                ))}
              </List>
              {related.length > 5 && (
                <Message>
                  Nog {related.length - 5} andere openbare ruimte(n).
                </Message>
              )}
            </Fragment>
          )}
        </Column>
        <Column>
          {selectedOisData && (
            <>
              <RedHeading as="h3">Betekenis</RedHeading>
              <List>
                {LIST_CONFIG.map(
                  (item) =>
                    selectedOisData[item.propName] && (
                      <MeaningListItem key={item.propName}>
                        <Cell>
                          {item.label === "Oorsprong" ? (
                            <Details title={item.label}>
                              Geeft de verbondenheid van de naam aan met
                              Nederland, Europa, de wereld, het koloniaal
                              verleden of ‘niet-Westers’ erfgoed.
                            </Details>
                          ) : (
                            item.label
                          )}
                        </Cell>
                        <Value>{selectedOisData[item.propName]}</Value>
                      </MeaningListItem>
                    )
                )}
              </List>
            </>
          )}
        </Column>
      </Row>
    </InfoContainer>
  );
};

export default Info;
