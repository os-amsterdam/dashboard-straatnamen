const FOCUSABLE_ELEMENTS = [
  // Extend to your needs
  'button',
]

const KeyboardKeys = {
  // Extend to your needs
  ArrowUp: 'ArrowUp',
  ArrowDown: 'ArrowDown',
  ArrowRight: 'ArrowRight',
  ArrowLeft: 'ArrowLeft',
  Escape: 'Escape',
  Enter: 'Enter',
  Space: 'Space',
  Home: 'Home',
  End: 'End',
}

/**
 * Focus on children with arrow keys and home / end buttons.
 *
 * @param ref Component ref
 * @param callback
 * @param options
 * @param options.rotating Jump to first item from last or vice versa
 * @param options.directChildrenOnly Useful if you don't want to focus on other focussable elements inside the child components of the ref
 * @param options.horizontally In case you need to navigate horizontally, using left / right arrow buttons
 */
const useTraverseList = (
  ref,
  callback,
  options = {
    rotating: false,
    directChildrenOnly: false,
    horizontally: false,
    skipElementWithClass: null
  },
  onEscape,
  onEnter
) => {
  const next = options?.horizontally ? KeyboardKeys.ArrowRight : KeyboardKeys.ArrowDown
  const previous = options?.horizontally ? KeyboardKeys.ArrowLeft : KeyboardKeys.ArrowUp
  const keyDown = (e) => {
    if (ref.current) {
      const element = ref.current

      const directChildSelector = options?.directChildrenOnly ? ':scope > ' : ''
      const focusableEls = Array.from(
        element.querySelectorAll(
          `${directChildSelector}${FOCUSABLE_ELEMENTS.join(`, ${directChildSelector}`)}`,
        ),
      )
      const activeElement = document.querySelector('.active')

      const getIndex = (el) => {
        return el && focusableEls.includes(el) ? focusableEls.indexOf(el) : 0
      }

      let el

      switch (e.key) {
        case next: {
          if (getIndex(activeElement) !== focusableEls.length - 1) {
            el = focusableEls[activeElement ? getIndex(activeElement) + 1 : 0]
            // If there is nothing focussed yet, set the focus on the first element
            if (activeElement && !focusableEls.includes(activeElement)) {
              ;[el] = focusableEls
            }
          } else if (options?.rotating) {
            ;[el] = focusableEls
          }

          break
        }

        case previous: {
          if (getIndex(activeElement) !== 0) {
            el = focusableEls[getIndex(activeElement) - 1]
          } else if (options?.rotating) {
            el = focusableEls[focusableEls.length - 1]
          }
          break
        }

        case KeyboardKeys.Escape: {
          onEscape()
          break
        }

        case KeyboardKeys.Enter: {
          onEnter(activeElement)
          focusableEls.forEach((el) => {
            el.classList.remove('active')
          })
          break
        }

        default:
      }

      if (
        (e.key === KeyboardKeys.ArrowDown ||
          e.key === KeyboardKeys.ArrowUp ||
          e.key === KeyboardKeys.ArrowLeft ||
          e.key === KeyboardKeys.ArrowRight
        ) &&
        el instanceof HTMLElement
      ) {
        callback(el, focusableEls)
        e.preventDefault()
      }
    }
  }

  return {
    keyDown,
  }
}

export default useTraverseList
