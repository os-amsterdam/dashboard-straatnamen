import { useState, useEffect } from 'react';
import styled from 'styled-components'
import { ViewerContainer } from '@amsterdam/asc-ui'
import { Map } from '@amsterdam/react-maps'
import { RDGeoJSON } from '@amsterdam/arm-core'
import Controls from '@amsterdam/arm-core/lib/components/Zoom'
import BaseLayer from '@amsterdam/arm-core/lib/components/BaseLayer'
import Scale from '@amsterdam/arm-core/lib/components/Scale'
import {
  DEFAULT_AMSTERDAM_LAYERS,
  DEFAULT_AMSTERDAM_MAPS_OPTIONS
} from '@amsterdam/arm-core/lib/constants'
import { rdToWgs84 } from './helpers'
// eslint-disable-next-line no-unused-vars
import { GestureHandling } from "leaflet-gesture-handling"
import 'leaflet/dist/leaflet.css'
import 'leaflet-gesture-handling/dist/leaflet-gesture-handling.css'

const BAG = 'https://api.data.amsterdam.nl/bag/v1.1/openbareruimte/'
const GEOSEARCH = 'https://api.data.amsterdam.nl/geosearch/search/'

const MapContainer = styled.div`
  position: relative;
  width: 100%;
  height: 400px;
`

const StyledMap = styled(Map)`
  position: relative;
  height: 100%;
  width: 100%;
`

const StyledViewerContainer = styled(ViewerContainer)`
  z-index: 400;
`

const convertCoords = (x, y) => rdToWgs84({ x: x, y: y })

const MapAms = ({ code, setSelected }) => {
  const [rdGeojson, setRDGeoJson] = useState()
  const [bbox, setBbox] = useState()
  const [map, setMap] = useState()
  const [clicked, setClicked] = useState()

  useEffect(() => {
    if (code) {
      let isMounted = true;

      fetch(`${BAG}${code}/`)
        .then(response => response.json())
        .then(json => {
          if (isMounted) {
            setRDGeoJson(json.geometrie)
            setBbox(json.bbox)
          }
        });

      return () => { isMounted = false; }
    }
  },[code])

  useEffect(() => {
    if (bbox) {
      const ne = convertCoords(bbox[0], bbox[1])
      const sw = convertCoords(bbox[2], bbox[3])

      // pan and zoom to feature bounds
      map.fitBounds(
        [
          [ne.latitude, ne.longitude],
          [sw.latitude, sw.longitude]
        ],
        {
          maxZoom: 10,
          pan: {
            duration: 0.3
          }
        }
      )
    }
  }, [bbox, map])

  useEffect(() => {
    if (clicked) {
      let isMounted = true;

      fetch(`${GEOSEARCH}?lat=${clicked.lat}&lon=${clicked.lng}&radius=50&item=openbareruimte`)
        .then(response => response.json())
        .then(json => {
          if (isMounted) {

            // Add 10m to 'Landschappelijk gebied', so other types within 10m have priority
            const weighted =
            json.features
            .map(item => (
              {
                properties: {
                  ...item.properties,
                  distance: item.properties.opr_type === 'Landschappelijk gebied' ? item.properties.distance + 10 : item.properties.distance
                }
              }
            )).sort((a,b) =>
              a.properties.distance-b.properties.distance
            )

            // Filter 'Watergraafsmeer' from results, because it overlaps with other areas
            const filtered = weighted.filter(item =>
              item.properties.display !== 'Watergraafsmeer'
            )

            const closest = filtered[0]?.properties
            closest && setSelected(
              {
                display: closest?.display,
                id: closest?.id
              }
            )
          }
        });

      return () => { isMounted = false; }
    }
  }, [clicked, setSelected])

  return (
    <MapContainer>
      <StyledMap
        events={{
          click: e => setClicked(e.latlng)
        }}
        options={{
          ...DEFAULT_AMSTERDAM_MAPS_OPTIONS,
          zoom: 8,
          minZoom: 6,
          attributionControl: false,
          gestureHandling: true
        }}
        setInstance={setMap}
      >
        <Scale
          options={{
            position: 'bottomleft',
            metric: true,
            imperial: false,
          }}
        />
        <StyledViewerContainer bottomRight={<Controls />} />
        <BaseLayer baseLayer={DEFAULT_AMSTERDAM_LAYERS[0].urlTemplate} />
        {rdGeojson ? <RDGeoJSON geometry={rdGeojson} /> : null}
      </StyledMap>
    </MapContainer>
  )
}

export default MapAms