import styled from 'styled-components'
import {
  themeColor,
  themeSpacing,
  Icon,
} from '@amsterdam/asc-ui'
import { ChevronDown } from '@amsterdam/asc-assets'
import LoadingCard from './LoadingCard'

const StyledCardSmall = styled.button`
  display: flex;
  align-items: center;
  position: relative;
  width: 100%;
  min-height: ${themeSpacing(21)};
  border: none;
  margin: 0;
  padding: 0;
  background-color: ${themeColor('tint', 'level2')};
  text-align: left;
  cursor: pointer;
  transition: transform 0.15s ease-in-out;
  transition: background-color 0.1s ease-in-out 0s;

  &:hover{
    background-color: ${themeColor('tint', 'level3')};
  }
`

const IconWrapper = styled.div`
  display: flex;
  align-items: center;
  border-left: 3px solid white;
  height: calc(100% - ${themeSpacing(10)});
  padding-left: ${themeSpacing(3)};
  padding-right: ${themeSpacing(4)};
`

const StyledIcon = styled(Icon)`
  transform: rotate(${({ isOpen }) => isOpen ? '180deg' : '0deg'});
  transition: transform 0.3s ease;
`

const CardSmall = ({ 
  id,
  isLoading,
  isExpanded,
  setExpandedCard,
  content
}) => {
  return (
    !isLoading ?
    <StyledCardSmall
      type="button"
      aria-expanded={isExpanded}
      onClick={() => isExpanded ? setExpandedCard() : setExpandedCard(id)}
    >
      {content}
      <IconWrapper>
        <StyledIcon size={20} inline isOpen={isExpanded}>
          <ChevronDown />
        </StyledIcon>
      </IconWrapper>
    </StyledCardSmall> : <LoadingCard />
  )
}

export default CardSmall