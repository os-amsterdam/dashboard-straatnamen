import React from 'react'
import styled, { keyframes } from 'styled-components'
import { themeColor } from '@amsterdam/asc-ui'

const LoadingKeyframes = ({ theme }) => keyframes`
  0% { background-color: ${themeColor('tint', 'level4')({ theme })} }
  50% { background-color: ${themeColor('tint', 'level2')({ theme })} }
  100% { background-color: ${themeColor('tint', 'level4')({ theme })} }
`

const StyledContainer = styled.div`
  height: 150px;
  animation-name: ${LoadingKeyframes};
  animation-duration: 2s;
  animation-timing-function: ease-in-out;
  animation-iteration-count: infinite;
`
const LoadingCard = () => <StyledContainer />

export default LoadingCard