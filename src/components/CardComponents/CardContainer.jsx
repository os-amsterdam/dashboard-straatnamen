import React from 'react'
import styled from 'styled-components'
import { breakpoint, themeSpacing } from '@amsterdam/asc-ui'

const StyledCardContainer = styled.div`
  display: inline-grid;
  vertical-align: top;
  width: calc(50% - ${themeSpacing(4)});
  margin-right: ${themeSpacing(8)};
  margin-bottom: ${themeSpacing(4)};

  &:nth-child(2n) {
    margin-right: auto;

    /* some styling for every second expanded card happens here */
    & > button + div {
      @media screen and ${breakpoint('min-width', 'laptop')} {
        margin-left: calc(-100% - ${themeSpacing(8)});
        &:before, &:after {
          left: 75%;
        }
      }
    }
  }

  @media screen and ${breakpoint('max-width', 'laptop')} {
    width: 100%;
    margin-right: auto;
  }

  /* IE11 hack */
  @media all and (-ms-high-contrast:none) {
    *::-ms-backdrop, & { display: inline-block; }
  }

  .print & {
    width: 100%;

    & > button + div {
      margin-left: 0;
      &:before, &:after {
          left: 50%;
        }
    }
  }
`

const CardContainer = ({ children }) => (
  <StyledCardContainer>
    {children}
  </StyledCardContainer>
)

export default CardContainer