import { memo } from 'react'
import {
  Row,
  Button,
  breakpoint,
  themeColor,
  themeSpacing
 } from '@amsterdam/asc-ui'
import { Close } from '@amsterdam/asc-assets'
import styled from 'styled-components'

const StyledCardExpanded = styled.div`
  position: relative;
  background-color: white;
  color: black;
  display: ${({ show }) => show ? 'block' : 'none'};
  margin-top: ${themeSpacing(6)};
  padding-top: ${themeSpacing(6)};
  padding-bottom: ${themeSpacing(6)};
  border: 2px solid ${themeColor('tint', 'level3')};

  &:after, &:before {
    bottom: 100%;
    left: 50%;
    border: solid transparent;
    content: '';
    height: 0;
    width: 0;
    position: absolute;
    pointer-events: none;
  }
  &:after {
    border-color: transparent;
    border-bottom-color: #ffffff;
    border-width: 17px;
    margin-left: -17px;
    margin-bottom: -1px;
  }
  &:before {
    border-color: transparent;
    border-bottom-color: ${themeColor('tint', 'level3')};
    border-width: 19px;
    margin-left: -19px;
  }

  /* with 2 cards in a row */
  @media screen and ${breakpoint('min-width', 'laptop')} {
    width: calc(200% + 32px);
    &:before, &:after{
      left: 25%;
    }
  }
`

const StyledRow = styled(Row)`
  margin-bottom: ${themeSpacing(2)};
`

const CardExpandedContent = memo(({ children, showCard, setExpandedCard }) => {
  return (
    <StyledCardExpanded show={showCard}>
      <StyledRow halign='flex-end'>
        <Button
          onClick={() => setExpandedCard()}
          size={30}
          variant='blank'
          iconSize={20}
          icon={<Close alt='Sluit extra informatie' title='Sluit extra informatie' />}
          type="button"
        />
      </StyledRow>
      {children}
    </StyledCardExpanded>
  )
})

const CardExpanded = ({
  isExpanded,
  setExpandedCard,
  children
}) => {
  return (
    <CardExpandedContent
      showCard={isExpanded}
      setExpandedCard={setExpandedCard}
    >
      {children}
    </CardExpandedContent>
  )
}

export default CardExpanded