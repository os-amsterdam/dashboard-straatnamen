import styled from 'styled-components'
import { themeSpacing } from '@amsterdam/asc-ui'

const StyledCardCollection = styled.div`
  width: 100%;
  padding-top: ${themeSpacing(4)};
`

const CardCollection = ({ children }) => (
  <StyledCardCollection>
    {children}
  </StyledCardCollection>
)

export default CardCollection