import { useState, useCallback } from 'react'
import {
  Button,
  themeSpacing,
  themeColor,
  Icon
} from '@amsterdam/asc-ui'
import { ChevronDown } from '@amsterdam/asc-assets'
import styled from 'styled-components'

const StyledButton = styled(Button)`
  color: black;
  font-weight: 300;

  & path {
    fill: black !important;
  }
  &:hover path {
    fill: ${themeColor('secondary')} !important;
  }
`

const Content = styled.div`
  display: ${({ isOpen }) => !isOpen && 'none'};
  margin-top: ${themeSpacing(2)};
  color: ${themeColor('tint', 'level5')};
`

const StyledIcon = styled(Icon)`
  transform: rotate(${({ isOpen }) => isOpen ? '180deg' : '0deg'});
  transition: transform 0.3s ease;
  padding-left: ${themeSpacing(1)};
  padding-right: ${themeSpacing(1)};
`

const Details = ({ children, title }) => {
  const [open, setOpen] = useState()

  const handleClick = useCallback(() => {
    setOpen(!open)
  }, [open])

  return (
    <>
      <StyledButton
        id='detail-button'
        aria-controls='detail-content'
        aria-expanded={open}
        type="button"
        variant="textButton"
        onClick={handleClick}
      >
        {title}
        <StyledIcon isOpen={open} size={14}>
          <ChevronDown />
        </StyledIcon>
      </StyledButton>
      <Content
        id='detail-content'
        aria-labelledby='detail-button'
        isOpen={open}
      >
        {children}
      </Content>
    </>
  )
}

export default Details