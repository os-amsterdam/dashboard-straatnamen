# Dashboard Straatnamen

This project is hosted on [amsterdam.nl](https://www.amsterdam.nl/bestuur-organisatie/organisatie/dii/basisinformatie/straten-amsterdam/straatnamenkaart-amsterdam/)

To run it locally, run `npm start` in the project directory:

This runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.

## Updating this project

There is no automated CI/CD pipeline for this project. In order to deploy changes, run `npm run build` and send a zip file with the build to <webteamcommunicatie@amsterdam.nl>

## Data sources

This project has three data sources:

- The BAG api, for the list of street names and their description. This is also the source for related streets ('Openbare ruimten vernoemd naar ...'). This data comes directly from the BAG and is updated and maintained there.
- Data file O&S, this is a static file which isn't currently updated. This file currently provides the fields 'Oorsprong', 'Reden naamgeving', 'Geboortejaar' and 'Sterfjaar'.
- Data file Van Gisteren agency, this is the source of the more verbose descriptions for some of the streets (59 at this time, 8-2-2023). This file is updated irregularly.
